﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour {

    [SerializeField]
    private UnityEngine.Events.UnityEvent IsGameEnd;


    //あたり判定ではみ出しチェック
    private void OnTriggerEnter2D(Collider2D collision)
    {
        IsGameEnd.Invoke();

    }
}
